import pygame as pg
from utils import path_sounds

class Music:
    volume = 0.5
    _piste_amplificator_ = 0
    _curent_piste_ = None
    
    def _load_(piste, ampli):
        if Music._curent_piste_ == piste:return
        Music._curent_piste_ = piste

        pg.mixer.music.stop()

        if piste is not None:
            pg.mixer.music.load(path_sounds + piste)
            Music._piste_amplificator_ = ampli
            Music.set_volume()
            pg.mixer.music.play(-1)


    def stop(): Music._load_(None, 0)
    def menu(): Music._load_("music_menu.mp3", 1)
    def game(): Music._load_("music_game.mp3", 1)


    def set_volume(volume = None):
        if volume is not None:
            Music.volume = volume
        pg.mixer.music.set_volume(Music.volume * Music._piste_amplificator_)
