import pygame as pg

pg.init()

info = pg.display.Info()
size_screen = info.current_w, info.current_h
CTE = (size_screen[0] * size_screen[1]) ** .5 / 100
del info


path_images = "image/"
path_fonts = "font/"
path_sounds = "musique/"

path_file_buildings = "buildings.csv"


def load_image(name):
    return pg.image.load(path_images + name)

def resize(surface, size):
    return pg.transform.scale(surface, size)

defaut_font = path_fonts + "Mantinia Regular.otf"

FORCE_ALL_RECT_VISIBLE = False

if __name__ == "__main__":
    import main
