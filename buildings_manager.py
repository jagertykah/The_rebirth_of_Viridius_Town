import numpy as np
from buildings import InstanciedBuilding, load_buildings
import pygame as pg

class Buildings_manager:
    def __init__(self, world):
        self.world = world

        load_buildings(world.ressource_manager)

        self._surface_fixed_ = pg.Surface(world.size_surf, pg.SRCALPHA)
        self._surface_varying_ = pg.Surface(world.size_surf, pg.SRCALPHA)
        self._merged_surface_ = pg.Surface(world.size_surf, pg.SRCALPHA)

        self.buildings = np.zeros((world.taille_grille, world.taille_grille), dtype = InstanciedBuilding)
        self.buildings[:,:] = None

        self._selected_building = None

    def add_building(self, instanciedBuilding):
        pos = instanciedBuilding.pos

        for i in range(instanciedBuilding.taille[0]):
            for j in range(instanciedBuilding.taille[1]):
                self.buildings[(pos[0]+i, pos[1]+j)] = instanciedBuilding

        self._surface_fixed_.blit(instanciedBuilding.image, instanciedBuilding.rect)
        self._update_merged_surface_()

    def _draw_mask_(self, instanciedBuilding, color = (255,255,255)):
        if instanciedBuilding is not None:
            x_, y_ = instanciedBuilding.rect.topleft
            mask_global = pg.mask.from_surface(instanciedBuilding.image)#.outline()
            for mask in mask_global.connected_components():
                line = mask.outline()
                line = [(x+x_, y+y_) for (x,y) in line]
                if len(line) >= 2:
                    pg.draw.polygon(self._surface_varying_, color, line, 1)

    def select_building(self, instanciedBuilding):
        self._draw_mask_(self._selected_building, (255, 255, 255, 0))
        self._selected_building = instanciedBuilding
        self._draw_mask_(self._selected_building, (255, 255, 255, 255))
        self._update_merged_surface_()

    def get_selected_building(self):
        return self._selected_building

    def remove_building(self, instanciedBuilding):
        pass

    def _update_merged_surface_(self):
        self._merged_surface_ = self._surface_fixed_.copy()
        self._merged_surface_.blit(self._surface_varying_, (0,0))

    def get_building(self, position):
        return self.buildings[position]

    def get_surf(self):
        return self._merged_surface_

if __name__ == "__main__":
    import main

    app = main.app
    world = app.game.world

    bm = Buildings_manager(world)
