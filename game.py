from music import Music
import pygame as pg
import utils
from ressource_manager import RessourceManager
from world import World

class Game:
    FPS = 60
    
    def __init__(self):
        self.clock = pg.time.Clock()
        self._running_ = False

        self.surface = pg.Surface(utils.size_screen)
        self.surface.fill((255, 0, 0))
        self.rect = self.surface.get_rect(topleft = (0,0))

        self.ressourceManager = RessourceManager()
        self.world = World(self.ressourceManager)


    def set_screen(self):
        pg.display.quit()
        self.screen = pg.display.set_mode(flags = pg.FULLSCREEN)
        self.world.set_screen(self.screen)

    def quit(self):
        self._running_ = False

    def react_events(self, events):
        for event in events:
            if event.used:
                continue
            if event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                self.quit()
                event.used = True

        self.world.react_events(events)
        
    def run(self):
        self._running_ = True
        Music.game()
        self.set_screen()
        
        while self._running_:
            self.screen.blit(self.surface, self.rect)
            self.world.update()

            events = pg.event.get()
            for e in events:
                e.used = False
            self.react_events(events)

            self.affiche_fps()
      
            pg.display.flip()
            self.clock.tick(self.FPS)

    def affiche_fps(self):
        fps = round(self.clock.get_fps())
        txt = pg.font.Font(utils.defaut_font, 20).render(f"FPS:{fps}", True, "white")
        rect = txt.get_rect(topleft = (30, 30))
        self.screen.blit(txt, rect)



if __name__ == "__main__":
    import main
    self = main.app.game
