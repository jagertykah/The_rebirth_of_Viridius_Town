import pygame as pg
import utils
from camera import Camera
import os
import numpy as np
import tiles
from buildings_manager import Buildings_manager
import projection

import buildings

class World:
    zoom = projection.zoom
    taille_grille = projection.taille_grille
    size_surf = projection.size_surf
    
    def __init__(self, ressource_manager):
        self.surface = pg.Surface(self.size_surf)#, pg.SRCALPHA)

        self.ressource_manager = ressource_manager

        self.tiles_builders = tiles.load_tiles()
        self.camera = Camera(self.surface, self.corrige_pos)


        self.buildings_manager = Buildings_manager(self)

        cabane = buildings.buildings[0].new((1,1))
        self.buildings_manager.add_building(cabane)

        maison = buildings.buildings[1].new((3,3))
        self.buildings_manager.add_building(maison)

        self.build_grille()
        self.build_ground()

        self.load()

    def load(self):
        return
        # TODO
        os.makedirs("save", exist_ok = True)
        if os.path.isfile("save/world") and False:
            with open("save/world", "r") as file:
                pass
        else:
#            self.buildings = np.zeros((projection.taille_grille, projection.taille_grille), dtype = object)
            pass

    def build_ground(self):
        self.surf_ground = pg.Surface(self.size_surf, pg.SRCALPHA)
        tile_builder = self.tiles_builders["block"]
        for i in range(self.taille_grille):
            for j in range(self.taille_grille):
                tile = tile_builder.new((i, j))
                self.surf_ground.blit(tile.image, tile.rect)


    def save(self):
        with open("save.world", "w") as file:
            pass

    def build_grille(self):
        self.surf_grille = pg.Surface(self.size_surf, pg.SRCALPHA)
        for i in range(projection.taille_grille+1):
            pg.draw.line(self.surf_grille, (0, 255, 0), self.grid2surf(i, 0), self.grid2surf(i, World.taille_grille))
        for j in range(projection.taille_grille+1):
            pg.draw.line(self.surf_grille, (0, 255, 0), self.grid2surf(0, j), self.grid2surf(World.taille_grille, j))
        
    def set_screen(self, screen):
        self.screen = screen

    def corrige_pos(self, rect):
        x, y = utils.size_screen[0]/2 - rect.topleft[0], utils.size_screen[1]/2 - rect.topleft[1]
        i, j = self.surf2grid(x, y)
        i_ = max(0, min(projection.taille_grille, i))
        j_ = max(0, min(projection.taille_grille, j))
        x_, y_ = self.grid2surf(i_, j_)
        dx, dy = x - x_, y - y_
        rect = rect.move(dx, dy)
        return rect
        
    def update(self):
        self.camera.update()
        self.screen.blit(self.surface, self.camera.rect)
        self.screen.blit(self.surf_ground, self.camera.rect)
        self.screen.blit(self.surf_grille, self.camera.rect)
        self.screen.blit(self.buildings_manager.get_surf(), self.camera.rect)

    def grid2surf(self, i, j):
        return projection.grid2surf(i, j)

    def surf2grid(self, x, y):
        return projection.surf2grid(x, y)

    def screen2surf(self, x_scr, y_scr):
        return (x_scr - self.camera.rect.topleft[0],
                y_scr - self.camera.rect.topleft[1])
    
    def surf2screen(self, x, y):
        return (x + self.camera.rect.topleft[0],
                x + self.camera.rect.topleft[1])

    def round_pos_grid(self, pos):
        i, j = int(pos[0]), int(pos[1])
        if 0 <= i < self.taille_grille and 0 <= j < self.taille_grille:
            return i, j
        return None

    def screen2grid_round(self, pos):
        return self.round_pos_grid(
            self.surf2grid(
                *self.screen2surf(
                    *pg.mouse.get_pos())))

    def react_events(self, events):
        for event in events:
            if event.used:
                continue
            if event.type == pg.MOUSEBUTTONDOWN:
                pos = self.screen2grid_round(pg.mouse.get_pos())
                b = self.buildings_manager.get_building(pos)
                self.buildings_manager.select_building(b)

    
if __name__ == "__main__":

    # vérification des changements de coordonnées
    from random import random
    print("initialisation de world pour calculs...")
    w = World(None)
    for _ in range(5000):
        coord = (200 * random() - 100, 200 * random() - 100)
        coord2 = w.surf2grid(*w.grid2surf(*coord))
        coord3 = w.grid2surf(*w.surf2grid(*coord))
        d = abs(coord[0]-coord2[0]) + abs(coord[0]-coord3[0]) + abs(coord2[0]-coord3[0]) + abs(coord[1]-coord2[1]) + abs(coord[1]-coord3[1]) + abs(coord2[1]-coord3[1])
        if d >= 1e-2:
            print(coord)
            break

    # lancement du jeu
    print("lancement du jeu")
    import main
    self = main.app.game.world
