import pygame as pg
from datetime import datetime
import pandas as pd
import tiles

debug = lambda *args:()
debug = print

def _float_(val):
    if type(val) == str:
        return float(val.replace(",", "."))
    else: return float(val)

class Building (tiles.Tile_builder):
    def __init__(self, name, ressource_manager, cost, prod, doc):
        super().__init__(name)
        self._cost_ = {str(key): _float_(val) for key, val in cost.items()}
        self._prod_ = {str(key): _float_(val) for key, val in prod.items()}
        self._ressource_manager_ = ressource_manager
        self.doc = doc

    def build(self, pos):
        tile = super().new(pos)
        if not self.is_affordable():
            raise Exception("Not enouth ressources to build")

        for ressource, cost in self._cost_.items():
            self._ressource_manager_.ressources[ressource] -= cost

        building = InstanciedBuilding(
            tile,
            self._ressource_manager_,
            self._prod_
            )
        return building

    def is_affordable(self):
        if self.name == "cabane" and False: # debug
            print("ressources", self._ressource_manager_.ressources)
            print("cost", self._cost_)
            print("")
            
        for ressource, cost in self._cost_.items():
            if self._ressource_manager_.ressources[ressource] < cost:
                return False
        return True
        

class InstanciedBuilding(tiles.Tile):
    def __init__(self, tile, ressource_manager, prod):
        super().init_from_tile(tile)
        self._prod_ = prod
        self._ressource_manager_ = ressource_manager
        
        self._last_update_ = datetime.now()

    def update(self):
        time_act = datetime.now()
        delay = min(5, (time_act - self._last_update_).total_seconds())
        self._last_update_ = time_act
        
        for ressource, quantity in self._prod_.items():
            self._ressource_manager_.ressources[ressource] += delay * quantity


def load_buildings(ressource_manager):
    global buildings
    if 'buildings' in globals():
        return buildings
    
    df = pd.read_csv("buildings.csv", sep = ";")

    buildings = []
    for i in df.index:
        ln = df.loc[i]
        cost = {
            "Wood":         ln["bois"],
            "Stone":        ln["pierre"],
            "Energy":       ln["energie"],
            "Gold":         ln["argent"],
            "Iron":         ln["fer"],
            "Searching":    ln["recherche minimale"],
            "Inhabitants":  -ln["habitants"],
            "CE":           ln["cout environnemental"],
            }
        prod = {
            "Wood":         ln["bois /s"],
            "Stone":        ln["pierre /s"],
            "Energy":       ln["energie /s"],
            "Gold":         ln["argent /s"],
            "Iron":         ln["fer /s"],
            "Searching":    ln["recherche /s"],
            "CE":           ln["cout environnemental /s"],
            }
        doc = ln["infos"]
        buildings.append(Building(
            ln["Nom"],
            ressource_manager,
            cost,
            prod,
            doc,
            ))

    return buildings

    # TODO next version
    # "Categorie;duree (en seconde);place;nourriture /s;"


if __name__ == "__main__":
    from ressource_manager import RessourceManager

    rm = RessourceManager()
    load_buildings(rm)
