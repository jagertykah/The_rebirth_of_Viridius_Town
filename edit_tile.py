from PIL import Image
import pygame as pg
import numpy as np
import os

os.makedirs("image/tile", exist_ok = True)

def _pilImageToSurface_(pilImage):
    return pg.image.fromstring(
        pilImage.tobytes(), pilImage.size, pilImage.mode).convert()

def _transform_image_(pilImageIn, new_size, pts_ref_out, pts_ref_in):
    A = []
    B = []
    for (x,y), (X,Y) in zip(pts_ref_in, pts_ref_out):
        A.append([X, Y, 1, 0, 0, 0, -x*X, -x*Y])
        B.append([x])
        A.append([0, 0, 0, X, Y, 1, -y*X, -y*Y])
        B.append([y])
    try:
        a, b, c, d, e, f, g, h = np.linalg.lstsq(A, B, rcond = None)[0].reshape((8,))
        return pilImageIn.transform(new_size, Image.PERSPECTIVE, (a,b,c,d,e,f,g,h), Image.BICUBIC)
    except:
        return None


def create_tile():
    name = input("nom de la tuile : ")
    caract_error = set(name.lower()).difference(set("abcdefghijklmnopqrstuvwxyz_ "))
    if caract_error:
        print(f"Erreur : caractères interdits : {caract_error}")
        return
    if os.path.isfile(f"image/tile/{name}.png"):
        confirm = input("    Tuile existante. Voulez-vous la remplacer ? (O/n) ")
        if confirm.lower() != "o":
            print("    Annulé")
            return
        else:
            print("    Remplacé")
    taille = input("taille de la tuile : ")
    try:
        taille = tuple(map(int, taille.split(",")))
    except:
        print("Erreur : format de taille incompatible. Format attendu : <N, N>")
        return
    if len(taille) != 2:
        print("Erreur : format de taille incompatible. Format attendu : <N, N>")
        return

    path_image = input("image : ")
    if not os.path.isfile(path_image):
        print("Erreur : ce n'est pas une image")
        return
    
    try:
        pilImage = Image.open(path_image)
    except:
        print("Erreur lors de la lecture de l'image")
        return

    try:
        resol = int(input("resolution d'une case : "))
    except:
        print("Erreur lors de la lecture de la résolution de l'image")
        return

    origine = (0, 0)
    size_out = (1000, 1000)
    _edit_tile_(name, taille, pilImage, resol, origine, size_out)

def _edit_tile_(name, taille, pilImage, resol, origine, size_out):
    pg.init()

    screen = pg.display.set_mode((1000, 500))

    def surfIn2img(x_surf, y_surf):
        x_img = (x_surf - transl_in[0]) / zoom_in
        y_img = (y_surf - transl_in[1]) / zoom_in
        return (x_img, y_img)
        
    def img2surfIn(x_img, y_img):
        x_surf = x_img * zoom_in + transl_in[0]
        y_surf = y_img * zoom_in + transl_in[1]
        return (x_surf, y_surf)

    def posToSurf(i, j):
        x = (i-j) * emprise_out + transl_out[0]
        y = (i+j)/2 * emprise_out + transl_out[1]
        return (x, y)

    def surfOut2img(x_surf, y_surf):
        x_img = (x_surf - transl_out[0]) * resol/emprise_out + origine[0]
        y_img = (y_surf - transl_out[1]) * resol/emprise_out + origine[1]
        return (x_img, y_img)

    def img2surfOut(x_img, y_img):
        x_surf = (x_img - origine[0]) * emprise_out/resol + transl_out[0]
        y_surf = (y_img - origine[1]) * emprise_out/resol + transl_out[1]
        return (x_surf, y_surf)
        
    emprise_out = 60
    zoom_in = 350 / max(_pilImageToSurface_(pilImage).get_size())
    transl_out = (250, 200)
    transl_in = (100, 100)

    pts_ref_out = [surfOut2img(*posToSurf(i, j)) for i in range(taille[0]+1) for j in range(taille[1]+1)]
    pts_ref_in = [pos for pos in pts_ref_out]
    
    run = True
    i_selected_point = None
    clock = pg.time.Clock()
    pos_clic = None
    verrouille = 0
    while run:
        # image in
        surf_in = pg.Surface((500, 500))
        surf_in.fill((50, 50, 50))

        pg_img_in = _pilImageToSurface_(pilImage)
        new_size = [zoom_in * s for s in pg_img_in.get_size()]
        pg_img_in = pg.transform.scale(pg_img_in, new_size)
        rect_img_in = pg_img_in.get_rect(topleft = transl_in)
        surf_in.blit(pg_img_in, rect_img_in)
        screen.blit(surf_in, (0,0))

        # image out
        surf_out = pg.Surface((500, 500))
        surf_out.fill((80, 80, 80))
        pts_ref = [ref for ref in zip(pts_ref_in, pts_ref_out) if not None in ref]
        pts_in, pts_out = [*zip(*pts_ref)]
        pilImageTransf = _transform_image_(pilImage, size_out, pts_out, pts_in)
        if pilImageTransf is not None:
            pg_img_out = _pilImageToSurface_(pilImageTransf)
            pg_img_out = pg.transform.scale(pg_img_out, [s / resol * emprise_out for s in pg_img_out.get_size()])
            rect = pg_img_out.get_rect(topleft = img2surfOut(0,0))
            surf_out.blit(pg_img_out, rect)
            
        # grille
        for i in range(taille[0]+1):
            pg.draw.line(surf_out, (0, 255, 0), posToSurf(i, 0), posToSurf(i, taille[1]))
        for j in range(taille[1]+1):
            pg.draw.line(surf_out, (0, 255, 0), posToSurf(0, j), posToSurf(taille[0], j))
        # affichage
        screen.blit(surf_out, (500, 0))

        # pts_ref
        surf_pts = pg.Surface((1000, 500), pg.SRCALPHA)
        for i_pt in range(len(pts_ref_in)):
            if pts_ref_in[i_pt] is not None:
                color = (255, 0, 0) if i_pt == i_selected_point else (0, 0, 255)
                pg.draw.circle(surf_pts, color, img2surfIn(*pts_ref_in[i_pt]), 3)
            
        for i_pt in range(len(pts_ref_out)):
            if pts_ref_out[i_pt] is not None:
                color = (255, 0, 0) if i_pt == i_selected_point else (0, 0, 255)
                coords = img2surfOut(*pts_ref_out[i_pt])
                coords = [coords[0] + 500, coords[1]]
                pg.draw.circle(surf_pts, color, coords, 3)

        if pos_clic is not None:
            pos_act = pg.mouse.get_pos()
            if abs(pos_clic[0] - pos_act[0]) + abs(pos_clic[1] - pos_act[1]) >= 2 and pos_clic[0] >= 500:
                x1, y1 = pos_act
                x2, y2 = pos_clic
                color = (255, 255, 0)
                pg.draw.line(surf_pts, color, (x1, y1), (x1, y2))
                pg.draw.line(surf_pts, color, (x1, y2), (x2, y2))
                pg.draw.line(surf_pts, color, (x2, y2), (x2, y1))
                pg.draw.line(surf_pts, color, (x2, y1), (x1, y1))

        screen.blit(surf_pts, (0, 0))
                
        # events
        for event in pg.event.get():
            if event.type == pg.QUIT:
                run = False

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_TAB:
                    if i_selected_point is not None:
                        i_selected_point += 1
                        if i_selected_point >= len(pts_ref_in) and i_selected_point >= len(pts_ref_out):
                            i_selected_point = 0
                if event.key == pg.K_BACKSPACE:
                    if len(pts_ref_in) > i_selected_point:
                        pts_ref_in[i_selected_point] = None
                elif event.key == pg.K_SPACE:
                    i_selected_point = None if i_selected_point is not None else 0
                if pg.mouse.get_pos()[0] >= 500:
                    if   event.key == pg.K_UP:    transl_out = (transl_out[0], transl_out[1]+5)
                    elif event.key == pg.K_DOWN:  transl_out = (transl_out[0], transl_out[1]-5)
                    elif event.key == pg.K_LEFT:  transl_out = (transl_out[0]+5, transl_out[1])
                    elif event.key == pg.K_RIGHT: transl_out = (transl_out[0]-5, transl_out[1])
                else:
                    pts_ref_in_surf = [img2surfIn(*ref) if ref is not None else None for ref in pts_ref_in]
                    if   event.key == pg.K_UP:    transl_in = (transl_in[0], transl_in[1]+5)
                    elif event.key == pg.K_DOWN:  transl_in = (transl_in[0], transl_in[1]-5)
                    elif event.key == pg.K_LEFT:  transl_in = (transl_in[0]+5, transl_in[1])
                    elif event.key == pg.K_RIGHT: transl_in = (transl_in[0]-5, transl_in[1])
                    if verrouille:
                        pts_ref_in = [surfIn2img(*ref) if ref is not None else None for ref in pts_ref_in_surf]
                if event.key == pg.K_RETURN:
                    pilImageTransf.save(f"image/tile/{name}.png")
                    with open(f"image/tile/{name}.txt", "w") as file:
                        file.write(str(origine)[1:-1] + "\n")
                        file.write(str(taille)[1:-1] + "\n")
                        file.write(str(resol) + "\n")
                    print("save")
                elif event.key == pg.K_ESCAPE:
                    pts_ref_in = [pos for pos in pts_ref_out]
                elif event.key == pg.K_RSHIFT or event.key == pg.K_LSHIFT:
                    verrouille += 1
            if event.type == pg.KEYUP:
                if event.key == pg.K_RSHIFT or event.key == pg.K_LSHIFT:
                    verrouille -= 1
            if event.type == pg.MOUSEBUTTONDOWN:
                pos_clic = pg.mouse.get_pos()
                if pos_clic[0] < 500 and i_selected_point is not None:
                    posIm = surfIn2img(*pos_clic)
                    pts_ref_in[i_selected_point] = posIm
            if event.type == pg.MOUSEWHEEL:
                if pg.mouse.get_pos()[0] >= 500:
                    if event.y ==  1: emprise_out /= 1.01
                    if event.y == -1: emprise_out *= 1.01
                else:
                    pts_ref_in_surf = [img2surfIn(*ref) if ref is not None else None for ref in pts_ref_in]
                    if event.y ==  1: zoom_in /= 1.01
                    if event.y == -1: zoom_in *= 1.01                    
                    if verrouille:
                        pts_ref_in = [surfIn2img(*ref) if ref is not None else None for ref in pts_ref_in_surf]
            if event.type == pg.MOUSEBUTTONUP:
                pos_declic = pg.mouse.get_pos()
                if abs(pos_clic[0] - pos_declic[0]) + abs(pos_clic[1] - pos_declic[1]) >= 2:
                    x_ori = min(pos_clic[0], pos_declic[0])
                    y_ori = min(pos_clic[1], pos_declic[1])

                    x_end = max(pos_clic[0], pos_declic[0])
                    y_end = max(pos_clic[1], pos_declic[1])

                    if x_ori >= 500:
                        pts_ref_out_surf = [img2surfOut(*ref) for ref in pts_ref_out]

                        move_ori = surfOut2img(x_ori-500, y_ori)
                        origine = (origine[0] - move_ori[0],
                                   origine[1] - move_ori[1])

                        pts_ref_out = [surfOut2img(*ref) for ref in pts_ref_out_surf]

                        size_out = surfOut2img(x_end-500, y_end)
                        size_out = (int(size_out[0]), int(size_out[1]))

                pos_clic = None
                
            if event.type == pg.MOUSEMOTION and pos_clic is not None and pos_clic[0] <= 500:
                pos_act = pg.mouse.get_pos()
                if i_selected_point is not None:
                    if pos_act[0] <= 500:
                        posIm = surfIn2img(*pos_act)
                        pts_ref_in[i_selected_point] = posIm
                else:
                    motion = [a-c for a,c in zip(pos_act, pos_clic)]

                    pts_ref_in_surf = [img2surfIn(*ref) if ref is not None else None for ref in pts_ref_in]

                    transl_in = (transl_in[0] + motion[0], transl_in[1] + motion[1])
                    if verrouille:
                        pts_ref_in = [surfIn2img(*ref) if ref is not None else None for ref in pts_ref_in_surf]
                    pos_clic = pos_act

        pg.display.flip()
        clock.tick(60)
    pg.quit()

def edit_existing_tile(name):
    with open(f"image/tile/{name}.txt", "r") as file:
        origine = tuple(map(float, file.readline().replace("\n","").split(",")))
        taille = tuple(map(int, file.readline().replace("\n","").split(",")))
        resol = float(file.readline().replace("\n", ""))
    pilImage = Image.open(f"image/tile/{name}.png")
    size_out = pilImage.size
    try:
        _edit_tile_(name, taille, pilImage, resol, origine, size_out)
    finally:
        pg.quit()

def list_tiles():
    l = os.listdir("image/tile")
    #print(l)
    s = {e.split(".")[0] for e in l if e[0] != "."}
    print("  - "+"\n  - ".join(s))


if __name__ == "__main__":
    print("""
==========  EDITEUR DE TUILES  ==========
pour editer une tuile :
>>> edit_existing_tile([nom])

pour creer une nouvelle tuile :
>>> create_tile()

tuiles existantes :""")
    list_tiles()
else:
    raise ImportError("No importation for this file")
