import utils
import pygame as pg
#from world import World
import os
import projection

"""
Tile_builder

attributs :
    image
    _origine_
    taille (ex : pour un batiment 2x1, (2,1))
methodes :
    new(zoom, pos) : genere une nouvelle image (type Tile). Donner pos = (i,j) pour placer. Return (image, rect)
    get_rect(pos)

Tile:
    name
    image
    rect
    taille
    pos
"""

class Tile_builder:
    def __init__(self, name):
        try:
            image = pg.image.load(f"image/tile/{name}.png")

            with open(f"image/tile/{name}.txt", "r") as file:
                origine = tuple(map(float, file.readline().replace("\n","").split(",")))
                self.taille = tuple(map(int, file.readline().replace("\n","").split(",")))
                resol = float(file.readline().replace("\n", ""))

        except FileNotFoundError:
            image = pg.Surface((5,5), pg.SRCALPHA)
            image.fill("red")
            origine = (0,0)
            self.taille = (1,1)
            resol = 20

        zoom = projection.zoom / resol

        self._origine_ = [o * zoom for o in origine]
        self.image = pg.transform.scale(image, [s * zoom for s in image.get_size()])
        self.name = name

    def get_rect(self, pos):
        topleft = [p-o for (p,o) in zip(projection.grid2surf(*pos), self._origine_)]
        rect = self.image.get_rect(topleft = topleft)
        return rect

    def new(self, pos, zoom = 1):
        # TODO gestion zoom
        image = self.image.copy()
        rect = self.get_rect(pos)
        return Tile(self.name, image, rect, self.taille, pos, self.get_rect)


class Tile:
    def __init__(self, name, image, rect, taille, pos, get_rect):
        self.name = name
        self.image = image
        self.rect = rect
        self.taille = taille
        self.pos = pos
        self.get_rect = get_rect

    def init_from_tile(self, tile):
        self.__init__(name = tile.name, image = tile.image, rect = tile.rect, taille = tile.taille, pos = tile.pos, get_rect = tile.get_rect)

    def set_rect(self, pos):
        self.rect = self.get_rect(pos)

def load_tiles():
    names_ext = [n.split(".") for n in os.listdir("image/tile")]
    names = [n[0] for n in names_ext if len(n) == 2 and n[1] == "png"]
    return {name:Tile_builder(name) for name in names}


if __name__ == "__main__":
    import main
