# projection

zoom = 50
taille_grille = 10
size_surf = (2 * (taille_grille+1) * zoom, (taille_grille + 5) * zoom)

def surf2grid(x, y):
    return ((y + x/2 - size_surf[0] / 4) / zoom - 3,
            (y - x/2 + size_surf[0] / 4) / zoom - 3)

def grid2surf(x_scr, y_scr):
    y = (x_scr + y_scr + 6) * zoom / 2
    x = (x_scr - y_scr) * zoom + size_surf[0] / 2
    return (x, y)


if __name__ == "__main__":
    from random import random

    def rand():
        return 1000 * random() * (-1 if random() < 0 else 1)

    for _ in range(5000):
        x = rand()
        y = rand()

        x_, y_ = grid2surf(*surf2grid(x, y))

        if (x-x_) ** 2 + (y-y_) ** 2 >= 1e-10:
            print(x, y)
