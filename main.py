import pygame as pg

from main_menu import MainMenu

###  mise à jour du chemin d'accès pour creation d'executables  ####################################
import os, sys
if getattr(sys, "frozen", False):
    os.chdir("/".join(sys.executable.split("/")[:-1]))

###  ajout d'un fichier d'erreurs  #################################################################
from datetime import datetime
def _write_error_(s, w = sys.stderr.write):
    w(s)
    n = datetime.now()
    date = "%2d:%2d:%2d.%6d    " % (n.hour, n.minute, n.second, n.microsecond)
    with open("errors.txt", "a") as f:
        s2 = s.replace("\n","\n"+len(date) * " ")
        f.write(f"{date}{s2}\n")
sys.stderr.write = _write_error_
with open("errors.txt","w"):pass   # efface le fichier
_write_error_("run\n", w = lambda s:())

###  métadonnées  ##################################################################################
with open("meta.txt", "w") as file:
    file.write("Project version : 1.0.1*\n")
    file.write("Date of starting version : 2022-05-18\n\n")
    file.write("DEV VERSION : if you find an error, please contact us at vincent.giudicelli@ensg.eu\n")




###  lancement  ####################################################################################
pg.init()
pg.mixer.init()
try:
    app = MainMenu()
    app.run()
finally:
    pg.quit()
